{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-15 09:30:22",
               "level" : 7,
               "x" : 0,
               "y" : 0
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P8",
         "happiness" : 0,
         "id" : 47615,
         "is_cap" : 1,
         "name" : "Lacuna",
         "orbit" : 1,
         "x" : 36,
         "y" : 2,
         "zone" : "0|0"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:19:17",
      "date_created" : "2020-11-15 09:30:21",
      "essentia" : 0,
      "id" : 1,
      "name" : "Lacuna Expanse Corp"
   },
   "glyph" : {},
   "plan" : {}
}
