{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-15 23:09:57",
               "level" : 5,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::University",
               "date_created" : "2020-11-16 07:31:19",
               "level" : 7,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Apple",
               "date_created" : "2020-11-16 07:34:30",
               "level" : 5,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Purification",
               "date_created" : "2020-11-16 07:35:01",
               "level" : 6,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 07:35:44",
               "level" : 7,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 07:36:33",
               "level" : 5,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 09:43:57",
               "level" : 6,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-20 07:59:51",
               "level" : 4,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Corn",
               "date_created" : "2020-11-22 09:45:29",
               "level" : 4,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-22 09:49:09",
               "level" : 3,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-22 11:34:25",
               "level" : 4,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::MissionCommand",
               "date_created" : "2021-04-18 06:00:33",
               "level" : 3,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Network19",
               "date_created" : "2021-04-18 17:20:14",
               "level" : 1,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Embassy",
               "date_created" : "2021-04-18 17:30:26",
               "level" : 1,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2021-04-18 17:45:32",
               "level" : 1,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-18 17:48:59",
               "level" : 2,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-18 17:49:39",
               "level" : 2,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2021-04-18 17:50:44",
               "level" : 1,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2021-04-18 18:16:23",
               "level" : 1,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2021-04-18 18:16:47",
               "level" : 2,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2021-04-18 18:17:08",
               "level" : 2,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2021-04-18 18:17:30",
               "level" : 2,
               "x" : -2,
               "y" : -5
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P4",
         "happiness" : -772,
         "id" : 11953,
         "is_cap" : 1,
         "name" : "Ushoette 3",
         "orbit" : 3,
         "x" : 405,
         "y" : -378,
         "zone" : "1|-1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:19:09",
      "date_created" : "2020-11-15 23:09:51",
      "essentia" : 5099,
      "id" : 2,
      "name" : "tle-sfs2x-www"
   },
   "glyph" : {},
   "plan" : {
      "Lacuna::DB::Result::Building::Permanent::Crater:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::KalavianRuins:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::SubspaceSupplyDepot:1:0" : {
         "class" : "Lacuna::DB::Result::Building::SubspaceSupplyDepot",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 2
      }
   }
}
