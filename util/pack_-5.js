{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-15 09:25:30",
               "level" : 1,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2021-04-25 17:43:15",
               "level" : 1,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2021-04-25 17:43:15",
               "level" : 30,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2021-04-25 17:43:15",
               "level" : 30,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2021-04-25 17:43:15",
               "level" : 30,
               "x" : -5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-25 17:43:16",
               "level" : 30,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-25 17:43:16",
               "level" : 30,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:16",
               "level" : 30,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2021-04-25 17:43:16",
               "level" : 30,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-25 17:43:16",
               "level" : 30,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-25 17:43:16",
               "level" : 30,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:16",
               "level" : 30,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:17",
               "level" : 30,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2021-04-25 17:43:17",
               "level" : 30,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2021-04-25 17:43:17",
               "level" : 30,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-25 17:43:17",
               "level" : 30,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-25 17:43:17",
               "level" : 30,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:17",
               "level" : 30,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:17",
               "level" : 30,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:17",
               "level" : 30,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTi",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTb",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTc",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTh",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTa",
               "date_created" : "2021-04-25 17:43:18",
               "level" : 30,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTd",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTg",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTf",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTe",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Transporter",
               "date_created" : "2021-04-25 17:43:19",
               "level" : 30,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Oversight",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:20",
               "level" : 30,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::TheftTraining",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::PoliticsTraining",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::MayhemTraining",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::IntelTraining",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:21",
               "level" : 30,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::SpaceJunkPark",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::PyramidJunkSculpture",
               "date_created" : "2021-04-25 17:43:22",
               "level" : 30,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2021-04-25 17:43:23",
               "level" : 30,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2021-04-25 17:43:23",
               "level" : 30,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2021-04-25 17:43:23",
               "level" : 30,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2021-04-25 17:43:23",
               "level" : 30,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2021-04-25 17:43:23",
               "level" : 30,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::PantheonOfHagness",
               "date_created" : "2021-04-25 17:43:23",
               "level" : 30,
               "x" : 5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:23",
               "level" : 30,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:23",
               "level" : 30,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-25 17:43:23",
               "level" : 30,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Park",
               "date_created" : "2021-04-25 17:49:06",
               "level" : 30,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TerraformingPlatform",
               "date_created" : "2021-04-25 17:51:37",
               "level" : 30,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TerraformingPlatform",
               "date_created" : "2021-04-25 17:52:18",
               "level" : 30,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TerraformingPlatform",
               "date_created" : "2021-04-25 17:52:40",
               "level" : 30,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::University",
               "date_created" : "2021-04-25 17:53:36",
               "level" : 30,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2021-04-25 17:54:37",
               "level" : 30,
               "x" : -4,
               "y" : -2
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P5",
         "happiness" : 131615900519,
         "id" : 34317,
         "is_cap" : 0,
         "name" : "Tchoegria 1",
         "orbit" : 1,
         "x" : 218,
         "y" : -140,
         "zone" : "0|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
               "date_created" : "2020-11-15 09:25:30",
               "level" : 1,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2021-04-25 17:23:44",
               "level" : 1,
               "x" : 0,
               "y" : 0
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P4",
         "happiness" : -145,
         "id" : 34318,
         "is_cap" : 0,
         "name" : "Tchoegria 2",
         "orbit" : 2,
         "x" : 219,
         "y" : -141,
         "zone" : "0|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2021-04-25 16:55:02",
               "level" : 15,
               "x" : 0,
               "y" : 0
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P9",
         "happiness" : -2,
         "id" : 34319,
         "is_cap" : 1,
         "name" : "Tchoegria 3",
         "orbit" : 3,
         "x" : 219,
         "y" : -143,
         "zone" : "0|0"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:20:06",
      "date_created" : "2021-04-25 16:55:02",
      "essentia" : 0,
      "id" : -5,
      "name" : "Cult of the Fissure"
   },
   "glyph" : {
      "goethite" : {
         "quantity" : 1,
         "type" : "goethite"
      }
   },
   "plan" : {}
}
