{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : -5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 5,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 30,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:23",
               "level" : 25,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:24",
               "level" : 25,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : -5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 20,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 10,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 25,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 30,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 30,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 03:21:25",
               "level" : 30,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 20,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 25,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 30,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 30,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 30,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 15,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 15,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 15,
               "x" : 4,
               "y" : -1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P7",
         "happiness" : 1012991843,
         "id" : 7076,
         "is_cap" : 0,
         "name" : "Oggehash 7",
         "orbit" : 7,
         "x" : 207,
         "y" : -427,
         "zone" : "0|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 03:21:09",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 03:21:09",
               "level" : 30,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-16 03:21:09",
               "level" : 5,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 03:21:10",
               "level" : 30,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 03:21:10",
               "level" : 30,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2020-11-16 03:21:10",
               "level" : 30,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2020-11-16 03:21:11",
               "level" : 30,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:11",
               "level" : 30,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:11",
               "level" : 30,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:11",
               "level" : 30,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:11",
               "level" : 30,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:11",
               "level" : 30,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:11",
               "level" : 25,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:11",
               "level" : 25,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : 1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : -4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:12",
               "level" : 25,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 20,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 10,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 25,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 03:21:13",
               "level" : 30,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 30,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 30,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 20,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 30,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 25,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 30,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 30,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 30,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 15,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 15,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 03:21:14",
               "level" : 15,
               "x" : -3,
               "y" : -2
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P11",
         "happiness" : 1013048760,
         "id" : 10615,
         "is_cap" : 0,
         "name" : "Agl Proallo Occ 7",
         "orbit" : 7,
         "x" : -409,
         "y" : -384,
         "zone" : "-1|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 5,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 30,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 25,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:35",
               "level" : 25,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:36",
               "level" : 25,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 20,
               "x" : -5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 10,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2020-11-16 03:21:37",
               "level" : 25,
               "x" : -5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 03:21:38",
               "level" : 30,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 03:21:38",
               "level" : 30,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 03:21:38",
               "level" : 30,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 03:21:38",
               "level" : 20,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 03:21:38",
               "level" : 30,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 03:21:38",
               "level" : 25,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 03:21:38",
               "level" : 30,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 03:21:38",
               "level" : 30,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2020-11-16 03:21:38",
               "level" : 30,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 03:21:39",
               "level" : 15,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 03:21:39",
               "level" : 15,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 03:21:39",
               "level" : 15,
               "x" : 4,
               "y" : 3
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P14",
         "happiness" : 1013048733,
         "id" : 15703,
         "is_cap" : 0,
         "name" : "Amow 7",
         "orbit" : 7,
         "x" : 267,
         "y" : -334,
         "zone" : "1|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 30,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 5,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 30,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 30,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 30,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 30,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 30,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 30,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 30,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:15",
               "level" : 30,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 30,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:16",
               "level" : 25,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 25,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 03:21:17",
               "level" : 20,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 10,
               "x" : 5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 25,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 30,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 30,
               "x" : -3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 30,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 20,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 30,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 25,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 30,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 30,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 30,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 15,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 15,
               "x" : -5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 03:21:18",
               "level" : 15,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2021-04-25 19:20:35",
               "level" : 5,
               "x" : -5,
               "y" : -5
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P8",
         "happiness" : 1013138971,
         "id" : 36938,
         "is_cap" : 0,
         "name" : "Ghou Oangoeg 7",
         "orbit" : 7,
         "x" : -357,
         "y" : -111,
         "zone" : "-1|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 03:21:26",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 5,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 30,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 25,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 25,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 25,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:27",
               "level" : 25,
               "x" : -4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : -5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:28",
               "level" : 25,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 25,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 25,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 25,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 25,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 25,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 25,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 25,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 25,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 20,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 10,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 25,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 30,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 30,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 30,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 03:21:29",
               "level" : 20,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 30,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 25,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 30,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 30,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 30,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 15,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 15,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 15,
               "x" : -3,
               "y" : 1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P1",
         "happiness" : 1012991841,
         "id" : 61584,
         "is_cap" : 0,
         "name" : "Ougai 7",
         "orbit" : 7,
         "x" : 190,
         "y" : 146,
         "zone" : "0|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 03:21:39",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 03:21:39",
               "level" : 30,
               "x" : 1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-16 03:21:39",
               "level" : 5,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 03:21:39",
               "level" : 30,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 03:21:39",
               "level" : 30,
               "x" : -5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 30,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 30,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 30,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 30,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 30,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 30,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 30,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 25,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 25,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 25,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 25,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 25,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 25,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:40",
               "level" : 25,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : -4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:41",
               "level" : 25,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 25,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 25,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 25,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 25,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 20,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 10,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 25,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 30,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 30,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 30,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 20,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 30,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 25,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 30,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 03:21:42",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2020-11-16 03:21:43",
               "level" : 30,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 03:21:43",
               "level" : 15,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 03:21:43",
               "level" : 15,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 03:21:43",
               "level" : 15,
               "x" : -1,
               "y" : 1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P3",
         "happiness" : 1013048731,
         "id" : 69812,
         "is_cap" : 0,
         "name" : "Sphio Iackloa 7",
         "orbit" : 7,
         "x" : 484,
         "y" : 229,
         "zone" : "1|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
               "date_created" : "2020-11-15 09:42:22",
               "level" : 1,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 03:18:37",
               "level" : 1,
               "x" : 0,
               "y" : 0
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P14",
         "happiness" : -3857,
         "id" : 72124,
         "is_cap" : 1,
         "name" : "Irvietre 3",
         "orbit" : 3,
         "x" : 345,
         "y" : 254,
         "zone" : "1|1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 03:21:19",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 03:21:19",
               "level" : 30,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-16 03:21:19",
               "level" : 5,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 03:21:19",
               "level" : 30,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 03:21:19",
               "level" : 30,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2020-11-16 03:21:19",
               "level" : 30,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2020-11-16 03:21:19",
               "level" : 30,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:19",
               "level" : 30,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:19",
               "level" : 30,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 30,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 30,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 30,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:20",
               "level" : 25,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : -4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 25,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 20,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 03:21:21",
               "level" : 10,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 25,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 30,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 30,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 30,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 20,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 30,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 25,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 30,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 30,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 30,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 15,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 15,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 03:21:22",
               "level" : 15,
               "x" : 1,
               "y" : 3
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P19",
         "happiness" : 1013115565,
         "id" : 80848,
         "is_cap" : 0,
         "name" : "Gradoupoe 7",
         "orbit" : 7,
         "x" : -490,
         "y" : 349,
         "zone" : "-1|1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 03:21:30",
               "level" : 30,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 5,
               "x" : -5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 30,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 30,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 30,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 30,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 30,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 30,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 30,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 30,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 25,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 25,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:31",
               "level" : 25,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:32",
               "level" : 25,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 25,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 25,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 25,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 25,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 25,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 25,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 25,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 25,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 20,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 10,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 25,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 30,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 30,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 03:21:33",
               "level" : 30,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 20,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 30,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 25,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 30,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 30,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 30,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 15,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 15,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 03:21:34",
               "level" : 15,
               "x" : -5,
               "y" : 3
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P17",
         "happiness" : 1013115541,
         "id" : 90191,
         "is_cap" : 0,
         "name" : "Lyo Teesphio 7",
         "orbit" : 7,
         "x" : 13,
         "y" : 444,
         "zone" : "0|1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:19:28",
      "date_created" : "2020-11-16 03:18:37",
      "essentia" : 4999941,
      "id" : -1,
      "name" : "S?b?n Demesne"
   },
   "glyph" : {},
   "plan" : {}
}
