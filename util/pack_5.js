{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
               "date_created" : "2020-11-15 09:11:53",
               "level" : 1,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
               "date_created" : "2020-11-15 09:11:53",
               "level" : 1,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 00:05:27",
               "level" : 4,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Apple",
               "date_created" : "2020-11-20 18:28:10",
               "level" : 2,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Purification",
               "date_created" : "2020-11-20 18:30:44",
               "level" : 2,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-20 20:48:06",
               "level" : 1,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Geo",
               "date_created" : "2020-11-20 20:51:32",
               "level" : 1,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-20 20:53:14",
               "level" : 1,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::University",
               "date_created" : "2020-11-20 20:55:16",
               "level" : 1,
               "x" : 0,
               "y" : 1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P6",
         "happiness" : 0,
         "id" : 466,
         "is_cap" : 1,
         "name" : "Bulltastic",
         "orbit" : 3,
         "x" : 464,
         "y" : -498,
         "zone" : "1|-1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:18:44",
      "date_created" : "2020-11-16 00:05:25",
      "essentia" : 100,
      "id" : 5,
      "name" : "StrongTow3r"
   },
   "glyph" : {},
   "plan" : {}
}
