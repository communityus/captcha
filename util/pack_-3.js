{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:29:11",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-16 01:29:11",
               "level" : 28,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:29:11",
               "level" : 30,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 30,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 30,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 30,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 30,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 30,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 30,
               "x" : -4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 11,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 12,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 20,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 30,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 10,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 30,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 25,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 30,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:29:12",
               "level" : 25,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 22,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 22,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 22,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:13",
               "level" : 25,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 25,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 30,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 30,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 30,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 30,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 30,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 20,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 20,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 20,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:14",
               "level" : 20,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:15",
               "level" : 20,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:15",
               "level" : 20,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:29:15",
               "level" : 20,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:15",
               "level" : 20,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:15",
               "level" : 20,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:15",
               "level" : 20,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:15",
               "level" : 20,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:16",
               "level" : 20,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:16",
               "level" : 20,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:16",
               "level" : 20,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:16",
               "level" : 20,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:16",
               "level" : 20,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:16",
               "level" : 20,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:17",
               "level" : 20,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:17",
               "level" : 20,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:17",
               "level" : 30,
               "x" : -5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:17",
               "level" : 30,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:17",
               "level" : 30,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:17",
               "level" : 30,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:18",
               "level" : 30,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:18",
               "level" : 30,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:18",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:18",
               "level" : 30,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:18",
               "level" : 30,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:18",
               "level" : 30,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:29:18",
               "level" : 10,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::MissionCommand",
               "date_created" : "2020-11-16 03:09:09",
               "level" : 1,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-25 19:56:15",
               "level" : 29,
               "x" : -2,
               "y" : -2
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P12",
         "happiness" : 5879775978068,
         "id" : 16357,
         "is_cap" : 0,
         "name" : "Slowmbeu 6",
         "orbit" : 6,
         "x" : -378,
         "y" : -328,
         "zone" : "-1|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:29:05",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-16 01:29:05",
               "level" : 28,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:29:05",
               "level" : 30,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 30,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 30,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 30,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 30,
               "x" : 1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 30,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 30,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 11,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 12,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 20,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 30,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 10,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:29:06",
               "level" : 30,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 25,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 30,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 25,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 22,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 22,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 22,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 25,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 25,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 25,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 25,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 25,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 25,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:07",
               "level" : 25,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 25,
               "x" : -5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 25,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 25,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 25,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 30,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 30,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 30,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 30,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 30,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 20,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 20,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:08",
               "level" : 20,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:09",
               "level" : 20,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 20,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 20,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 20,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 20,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 30,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 30,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 30,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 30,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:10",
               "level" : 30,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:11",
               "level" : 30,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:11",
               "level" : 30,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:11",
               "level" : 30,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:11",
               "level" : 30,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:29:11",
               "level" : 10,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-25 19:56:15",
               "level" : 29,
               "x" : -4,
               "y" : 2
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P5",
         "happiness" : 5878778937124,
         "id" : 16767,
         "is_cap" : 1,
         "name" : "Eph Proaggee Aex 5",
         "orbit" : 5,
         "x" : 478,
         "y" : -328,
         "zone" : "1|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:29:33",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-16 01:29:33",
               "level" : 28,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:29:34",
               "level" : 30,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:29:34",
               "level" : 30,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:29:34",
               "level" : 30,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:29:34",
               "level" : 30,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:29:35",
               "level" : 30,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2020-11-16 01:29:35",
               "level" : 30,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:29:35",
               "level" : 30,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:29:35",
               "level" : 11,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2020-11-16 01:29:35",
               "level" : 12,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2020-11-16 01:29:36",
               "level" : 20,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:29:36",
               "level" : 30,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 01:29:36",
               "level" : 10,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:29:36",
               "level" : 30,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:29:36",
               "level" : 25,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:29:36",
               "level" : 30,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:29:37",
               "level" : 25,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:37",
               "level" : 22,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:37",
               "level" : 22,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:37",
               "level" : 22,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:38",
               "level" : 25,
               "x" : -3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:38",
               "level" : 25,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:38",
               "level" : 25,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:38",
               "level" : 25,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:38",
               "level" : 25,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:39",
               "level" : 25,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:39",
               "level" : 25,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:39",
               "level" : 25,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:39",
               "level" : 25,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:39",
               "level" : 25,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 25,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 30,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 30,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 30,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 30,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 30,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 20,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 20,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 20,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 20,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 20,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:40",
               "level" : 20,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 20,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 30,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:41",
               "level" : 30,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 30,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 30,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 30,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 30,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 30,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 30,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 30,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 10,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-25 19:56:15",
               "level" : 29,
               "x" : 3,
               "y" : -1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P15",
         "happiness" : 5879766316195,
         "id" : 19390,
         "is_cap" : 0,
         "name" : "Schiesseuhl 6",
         "orbit" : 6,
         "x" : -62,
         "y" : -296,
         "zone" : "0|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:29:42",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 28,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 30,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 30,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 30,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 30,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 30,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 30,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 30,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 11,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 12,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 20,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 30,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 10,
               "x" : -5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 30,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:29:43",
               "level" : 25,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 30,
               "x" : -3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 22,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 22,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 22,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:44",
               "level" : 25,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 25,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 25,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 30,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 30,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 30,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 30,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 30,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 20,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 20,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 20,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 20,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 20,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:45",
               "level" : 20,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : -4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : 1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:46",
               "level" : 20,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 20,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 30,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:29:47",
               "level" : 10,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-25 19:56:15",
               "level" : 29,
               "x" : -2,
               "y" : -1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P16",
         "happiness" : 5879763700809,
         "id" : 30456,
         "is_cap" : 0,
         "name" : "Groewostai 5",
         "orbit" : 5,
         "x" : 49,
         "y" : -184,
         "zone" : "0|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:29:18",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-16 01:29:18",
               "level" : 28,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 11,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 12,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 20,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 10,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 25,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:29:19",
               "level" : 30,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 25,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 22,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 22,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 22,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 25,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 25,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 25,
               "x" : -5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 25,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 25,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 25,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 25,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:20",
               "level" : 25,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:21",
               "level" : 25,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:21",
               "level" : 25,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2020-11-16 01:29:21",
               "level" : 25,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:29:21",
               "level" : 30,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:29:22",
               "level" : 30,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:29:22",
               "level" : 30,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:29:22",
               "level" : 30,
               "x" : -5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:29:23",
               "level" : 30,
               "x" : -5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:23",
               "level" : 20,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:23",
               "level" : 20,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:23",
               "level" : 20,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:23",
               "level" : 20,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:23",
               "level" : 20,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:23",
               "level" : 20,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 20,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:24",
               "level" : 30,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 30,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 30,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 30,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 30,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 30,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 30,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 30,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 30,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 30,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:29:25",
               "level" : 10,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-25 19:56:16",
               "level" : 29,
               "x" : 2,
               "y" : 4
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P4",
         "happiness" : 5879773877601,
         "id" : 35415,
         "is_cap" : 0,
         "name" : "Boegg 6",
         "orbit" : 6,
         "x" : -485,
         "y" : -128,
         "zone" : "-1|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 28,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 30,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 30,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 30,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 30,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 30,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 30,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 30,
               "x" : 1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 11,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 12,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 20,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:29:57",
               "level" : 30,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 10,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 30,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 25,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 30,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 25,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 22,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 22,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 22,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 25,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 25,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 25,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 25,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 25,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 25,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:58",
               "level" : 25,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 25,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 25,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 25,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 25,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 30,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 30,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 30,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 30,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 30,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 20,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 20,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 20,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:59",
               "level" : 20,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:30:00",
               "level" : 20,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:01",
               "level" : 30,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:01",
               "level" : 30,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:01",
               "level" : 30,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:01",
               "level" : 30,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:01",
               "level" : 30,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:01",
               "level" : 30,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:01",
               "level" : 30,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:01",
               "level" : 30,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:01",
               "level" : 30,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:02",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:30:02",
               "level" : 10,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-25 19:56:16",
               "level" : 29,
               "x" : -2,
               "y" : -5
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P13",
         "happiness" : 5879757040404,
         "id" : 67334,
         "is_cap" : 0,
         "name" : "Prio Oufleak Snea 5",
         "orbit" : 5,
         "x" : 358,
         "y" : 203,
         "zone" : "1|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:30:02",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-16 01:30:02",
               "level" : 28,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:30:02",
               "level" : 30,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:30:02",
               "level" : 30,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:30:02",
               "level" : 30,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:30:02",
               "level" : 30,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:30:02",
               "level" : 30,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 30,
               "x" : -5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 30,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 11,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 12,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 20,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 30,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 10,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 30,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 25,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 30,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 25,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 22,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 22,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 22,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:03",
               "level" : 25,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 25,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 30,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:30:04",
               "level" : 30,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 30,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 30,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 30,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 20,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 20,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 20,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 20,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 20,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 20,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 20,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 20,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:30:05",
               "level" : 20,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:30:06",
               "level" : 20,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:30:06",
               "level" : 20,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:30:06",
               "level" : 20,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:30:06",
               "level" : 20,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:30:06",
               "level" : 20,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:30:06",
               "level" : 20,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:30:06",
               "level" : 20,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:30:06",
               "level" : 20,
               "x" : -5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 20,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 20,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 30,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 30,
               "x" : -4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 30,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 30,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 30,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 30,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 30,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 30,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:07",
               "level" : 30,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:30:08",
               "level" : 30,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:30:08",
               "level" : 10,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-25 19:56:16",
               "level" : 29,
               "x" : 5,
               "y" : 5
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P16",
         "happiness" : 5879756169274,
         "id" : 90828,
         "is_cap" : 0,
         "name" : "Eest Eettiess 6",
         "orbit" : 6,
         "x" : 320,
         "y" : 448,
         "zone" : "1|1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 28,
               "x" : -5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 30,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 30,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 30,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 30,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 30,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 30,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 30,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 11,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 12,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 20,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:29:26",
               "level" : 30,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 10,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 30,
               "x" : -3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 25,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 30,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 25,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 22,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 22,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 22,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 25,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 25,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 25,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 25,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:27",
               "level" : 25,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:28",
               "level" : 25,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:28",
               "level" : 25,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:28",
               "level" : 25,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:28",
               "level" : 25,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:28",
               "level" : 25,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2020-11-16 01:29:28",
               "level" : 25,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:29:28",
               "level" : 30,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 30,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 30,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 30,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 30,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 20,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 20,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 20,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 20,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 20,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 20,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:29:29",
               "level" : 20,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:30",
               "level" : 20,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:30",
               "level" : 20,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:30",
               "level" : 20,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:30",
               "level" : 20,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:30",
               "level" : 20,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:30",
               "level" : 20,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:30",
               "level" : 20,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:30",
               "level" : 20,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:30",
               "level" : 20,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:31",
               "level" : 20,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:31",
               "level" : 20,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:31",
               "level" : 20,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:31",
               "level" : 30,
               "x" : 5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:31",
               "level" : 30,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:31",
               "level" : 30,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:31",
               "level" : 30,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:31",
               "level" : 30,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:32",
               "level" : 30,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:32",
               "level" : 30,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:32",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:32",
               "level" : 30,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:32",
               "level" : 30,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:29:33",
               "level" : 10,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-25 19:56:16",
               "level" : 29,
               "x" : 3,
               "y" : -2
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P20",
         "happiness" : 5879771347840,
         "id" : 90935,
         "is_cap" : 0,
         "name" : "Kloowiesl 6",
         "orbit" : 6,
         "x" : -455,
         "y" : 452,
         "zone" : "-1|1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 28,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 30,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 30,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 30,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 30,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 30,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 30,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 30,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 11,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 12,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2020-11-16 01:29:48",
               "level" : 20,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:29:49",
               "level" : 30,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2020-11-16 01:29:49",
               "level" : 10,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:29:49",
               "level" : 30,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:29:49",
               "level" : 25,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:29:49",
               "level" : 30,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:29:49",
               "level" : 25,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:49",
               "level" : 22,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:49",
               "level" : 22,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:29:49",
               "level" : 22,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:50",
               "level" : 25,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:50",
               "level" : 25,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:50",
               "level" : 25,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:50",
               "level" : 25,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:50",
               "level" : 25,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:50",
               "level" : 25,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:50",
               "level" : 25,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:50",
               "level" : 25,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:50",
               "level" : 25,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:29:51",
               "level" : 25,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2020-11-16 01:29:51",
               "level" : 25,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:29:51",
               "level" : 30,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:29:52",
               "level" : 30,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:29:52",
               "level" : 30,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:29:52",
               "level" : 30,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:29:53",
               "level" : 30,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:53",
               "level" : 20,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Exchanger",
               "date_created" : "2020-11-16 01:29:53",
               "level" : 20,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:54",
               "level" : 20,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:54",
               "level" : 20,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
               "date_created" : "2020-11-16 01:29:54",
               "level" : 20,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Root",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Fusion",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Production",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:55",
               "level" : 20,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 20,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 30,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:29:56",
               "level" : 10,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-25 19:56:16",
               "level" : 29,
               "x" : -3,
               "y" : -5
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P12",
         "happiness" : 5879762493678,
         "id" : 92639,
         "is_cap" : 0,
         "name" : "Mieri 5",
         "orbit" : 5,
         "x" : 125,
         "y" : 466,
         "zone" : "0|1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:19:47",
      "date_created" : "2020-11-16 01:29:04",
      "essentia" : 4889,
      "id" : -3,
      "name" : "Trelvestian Sveitarfélagi"
   },
   "glyph" : {},
   "plan" : {}
}
