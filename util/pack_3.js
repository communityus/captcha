{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-15 23:14:55",
               "level" : 4,
               "x" : 0,
               "y" : 0
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P15",
         "happiness" : 0,
         "id" : 78988,
         "is_cap" : 1,
         "name" : "Ceu Xuippoo 3",
         "orbit" : 3,
         "x" : -337,
         "y" : 326,
         "zone" : "-1|1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:19:02",
      "date_created" : "2020-11-15 23:14:52",
      "essentia" : 100,
      "id" : 3,
      "name" : "sfs2x-lacuna-client"
   },
   "glyph" : {},
   "plan" : {}
}
