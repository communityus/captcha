{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2021-04-24 23:42:34",
               "level" : 22,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TerraformingPlatform",
               "date_created" : "2021-04-24 23:47:54",
               "level" : 29,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2021-04-24 23:50:18",
               "level" : 21,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Corn",
               "date_created" : "2021-04-24 23:51:57",
               "level" : 14,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2021-04-24 23:52:09",
               "level" : 15,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2021-04-24 23:52:21",
               "level" : 13,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2021-04-24 23:52:51",
               "level" : 13,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::CornMeal",
               "date_created" : "2021-04-24 23:54:38",
               "level" : 14,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Lapis",
               "date_created" : "2021-04-24 23:54:52",
               "level" : 14,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Wheat",
               "date_created" : "2021-04-24 23:55:00",
               "level" : 14,
               "x" : -5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Ministry",
               "date_created" : "2021-04-24 23:55:09",
               "level" : 15,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Purification",
               "date_created" : "2021-04-24 23:55:43",
               "level" : 11,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-24 23:57:54",
               "level" : 5,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-24 23:58:40",
               "level" : 5,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2021-04-24 23:59:35",
               "level" : 5,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::MissionCommand",
               "date_created" : "2021-04-24 23:59:56",
               "level" : 5,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Network19",
               "date_created" : "2021-04-25 00:00:15",
               "level" : 5,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Embassy",
               "date_created" : "2021-04-25 00:00:52",
               "level" : 5,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2021-04-25 00:01:30",
               "level" : 5,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2021-04-25 00:07:07",
               "level" : 11,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::DistributionCenter",
               "date_created" : "2021-04-25 00:07:24",
               "level" : 5,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Transporter",
               "date_created" : "2021-04-25 00:08:04",
               "level" : 5,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TerraformingPlatform",
               "date_created" : "2021-04-25 00:12:31",
               "level" : 22,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::PantheonOfHagness",
               "date_created" : "2021-04-25 00:13:36",
               "level" : 29,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Pie",
               "date_created" : "2021-04-25 00:18:37",
               "level" : 14,
               "x" : -5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Bread",
               "date_created" : "2021-04-25 00:18:43",
               "level" : 14,
               "x" : -5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Park",
               "date_created" : "2021-04-25 00:19:49",
               "level" : 15,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::ThemePark",
               "date_created" : "2021-04-25 00:19:56",
               "level" : 15,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2021-04-25 00:20:16",
               "level" : 1,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2021-04-25 01:11:59",
               "level" : 30,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2021-04-25 01:12:06",
               "level" : 30,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2021-04-25 01:12:16",
               "level" : 30,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2021-04-25 01:12:31",
               "level" : 31,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SupplyPod",
               "date_created" : "2021-04-25 03:46:55",
               "level" : 15,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SubspaceSupplyDepot",
               "date_created" : "2021-04-25 03:54:17",
               "level" : 1,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
               "date_created" : "2021-04-25 03:58:27",
               "level" : 30,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2021-04-25 04:27:30",
               "level" : 13,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:14:05",
               "level" : 30,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:15:28",
               "level" : 30,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:16:40",
               "level" : 30,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:33:48",
               "level" : 30,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:34:21",
               "level" : 30,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:35:33",
               "level" : 30,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:35:58",
               "level" : 30,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:36:15",
               "level" : 30,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:36:34",
               "level" : 30,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:36:59",
               "level" : 30,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:37:16",
               "level" : 30,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:37:31",
               "level" : 30,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:37:46",
               "level" : 30,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:38:14",
               "level" : 30,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:38:26",
               "level" : 30,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:38:47",
               "level" : 30,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:44:11",
               "level" : 30,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:44:22",
               "level" : 30,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:44:37",
               "level" : 30,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:45:26",
               "level" : 30,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:47:19",
               "level" : 30,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:47:41",
               "level" : 30,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:47:54",
               "level" : 30,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-25 05:49:37",
               "level" : 30,
               "x" : 3,
               "y" : -3
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P8",
         "happiness" : -3153218,
         "id" : 52906,
         "is_cap" : 0,
         "name" : "Ournoo 2",
         "orbit" : 7,
         "x" : -500,
         "y" : -245,
         "zone" : "-1|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2021-04-24 04:04:34",
               "level" : 19,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TerraformingPlatform",
               "date_created" : "2021-04-24 05:07:13",
               "level" : 22,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-24 05:08:48",
               "level" : 5,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2021-04-24 05:09:52",
               "level" : 6,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2021-04-24 05:15:38",
               "level" : 16,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Park",
               "date_created" : "2021-04-24 06:19:58",
               "level" : 3,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::ThemePark",
               "date_created" : "2021-04-24 06:20:13",
               "level" : 3,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2021-04-24 06:22:01",
               "level" : 6,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Waste",
               "date_created" : "2021-04-24 06:22:31",
               "level" : 5,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Purification",
               "date_created" : "2021-04-24 06:23:13",
               "level" : 6,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2021-04-24 06:26:33",
               "level" : 11,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2021-04-24 16:56:47",
               "level" : 1,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Network19",
               "date_created" : "2021-04-24 17:34:02",
               "level" : 2,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2021-04-24 17:38:21",
               "level" : 1,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2021-04-24 20:21:46",
               "level" : 16,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2021-04-24 20:23:20",
               "level" : 9,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2021-04-24 20:41:34",
               "level" : 30,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2021-04-24 23:18:06",
               "level" : 1,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2021-04-24 23:18:17",
               "level" : 1,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-24 23:21:02",
               "level" : 1,
               "x" : -1,
               "y" : -1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P1",
         "happiness" : 95510609,
         "id" : 76156,
         "is_cap" : 0,
         "name" : "Fra Clarvoe 1",
         "orbit" : 1,
         "x" : -263,
         "y" : 302,
         "zone" : "-1|1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2021-04-18 04:32:04",
               "level" : 30,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::University",
               "date_created" : "2021-04-18 05:39:23",
               "level" : 30,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTc",
               "date_created" : "2021-04-24 01:41:09",
               "level" : 30,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTd",
               "date_created" : "2021-04-24 01:41:31",
               "level" : 30,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTe",
               "date_created" : "2021-04-24 01:41:47",
               "level" : 30,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTb",
               "date_created" : "2021-04-24 01:42:07",
               "level" : 30,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTa",
               "date_created" : "2021-04-24 01:42:29",
               "level" : 30,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTf",
               "date_created" : "2021-04-24 01:42:55",
               "level" : 30,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTi",
               "date_created" : "2021-04-24 01:43:39",
               "level" : 30,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTh",
               "date_created" : "2021-04-24 01:43:59",
               "level" : 30,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTg",
               "date_created" : "2021-04-24 01:44:23",
               "level" : 30,
               "x" : -3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2021-04-24 02:07:13",
               "level" : 1,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2021-04-24 02:07:57",
               "level" : 1,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
               "date_created" : "2021-04-24 02:08:34",
               "level" : 1,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2021-04-24 02:09:29",
               "level" : 1,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
               "date_created" : "2021-04-24 02:10:45",
               "level" : 1,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2021-04-24 02:11:16",
               "level" : 1,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TempleOfTheDrajilites",
               "date_created" : "2021-04-24 02:11:51",
               "level" : 1,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
               "date_created" : "2021-04-24 02:12:21",
               "level" : 1,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-24 02:14:00",
               "level" : 1,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2021-04-24 02:15:00",
               "level" : 1,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2021-04-24 02:15:57",
               "level" : 1,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::LibraryOfJith",
               "date_created" : "2021-04-24 02:18:48",
               "level" : 1,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2021-04-24 02:19:38",
               "level" : 1,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2021-04-24 02:20:08",
               "level" : 1,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2021-04-24 02:20:31",
               "level" : 1,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::PantheonOfHagness",
               "date_created" : "2021-04-24 02:20:56",
               "level" : 1,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2021-04-24 02:21:25",
               "level" : 1,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2021-04-24 02:21:59",
               "level" : 1,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
               "date_created" : "2021-04-24 02:22:20",
               "level" : 1,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
               "date_created" : "2021-04-24 02:49:13",
               "level" : 30,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
               "date_created" : "2021-04-24 02:49:40",
               "level" : 30,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach3",
               "date_created" : "2021-04-24 02:50:09",
               "level" : 30,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach9",
               "date_created" : "2021-04-24 02:50:48",
               "level" : 30,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach12",
               "date_created" : "2021-04-24 02:52:14",
               "level" : 30,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
               "date_created" : "2021-04-24 02:52:39",
               "level" : 30,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach8",
               "date_created" : "2021-04-24 02:53:04",
               "level" : 30,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
               "date_created" : "2021-04-24 02:53:27",
               "level" : 30,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
               "date_created" : "2021-04-24 02:54:15",
               "level" : 30,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach10",
               "date_created" : "2021-04-24 02:54:39",
               "level" : 30,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
               "date_created" : "2021-04-24 02:55:01",
               "level" : 30,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2021-04-24 03:00:33",
               "level" : 10,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-24 03:01:22",
               "level" : 11,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-24 03:02:02",
               "level" : 11,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2021-04-24 03:04:08",
               "level" : 5,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2021-04-24 03:26:30",
               "level" : 12,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Ministry",
               "date_created" : "2021-04-24 03:29:04",
               "level" : 13,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Capitol",
               "date_created" : "2021-04-24 03:34:30",
               "level" : 11,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Embassy",
               "date_created" : "2021-04-24 03:34:51",
               "level" : 7,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2021-04-24 03:36:37",
               "level" : 5,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2021-04-24 03:38:13",
               "level" : 10,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::IntelTraining",
               "date_created" : "2021-04-24 03:39:08",
               "level" : 6,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::PoliticsTraining",
               "date_created" : "2021-04-24 03:39:33",
               "level" : 6,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::MissionCommand",
               "date_created" : "2021-04-24 03:40:57",
               "level" : 3,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::TheftTraining",
               "date_created" : "2021-04-24 03:41:35",
               "level" : 17,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::MayhemTraining",
               "date_created" : "2021-04-24 03:42:01",
               "level" : 18,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Network19",
               "date_created" : "2021-04-24 03:42:37",
               "level" : 2,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2021-04-24 03:43:15",
               "level" : 6,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2021-04-24 03:43:34",
               "level" : 8,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2021-04-24 04:02:53",
               "level" : 20,
               "x" : -2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::EssentiaVein",
               "date_created" : "2021-04-24 04:11:14",
               "level" : 30,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::MercenariesGuild",
               "date_created" : "2021-04-24 04:35:12",
               "level" : 4,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2021-04-24 04:35:31",
               "level" : 16,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::GasGiantLab",
               "date_created" : "2021-04-24 04:46:50",
               "level" : 21,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::TerraformingLab",
               "date_created" : "2021-04-24 04:49:15",
               "level" : 21,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2021-04-24 05:47:33",
               "level" : 23,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SSLa",
               "date_created" : "2021-04-24 05:51:07",
               "level" : 4,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SSLb",
               "date_created" : "2021-04-24 05:51:31",
               "level" : 4,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SSLc",
               "date_created" : "2021-04-24 05:52:49",
               "level" : 4,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SSLd",
               "date_created" : "2021-04-24 05:53:21",
               "level" : 4,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-24 19:01:48",
               "level" : 12,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2021-04-24 20:37:58",
               "level" : 16,
               "x" : 3,
               "y" : 2
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P18",
         "happiness" : 28922676651,
         "id" : 76629,
         "is_cap" : 1,
         "name" : "Eandl Aehlowgr An 3",
         "orbit" : 3,
         "x" : -253,
         "y" : 303,
         "zone" : "-1|1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:09:06",
      "date_created" : "2021-04-18 04:32:00",
      "essentia" : 371,
      "id" : 7,
      "name" : "ninjafootball07"
   },
   "glyph" : {
      "anthracite" : {
         "quantity" : 1,
         "type" : "anthracite"
      },
      "bauxite" : {
         "quantity" : 3,
         "type" : "bauxite"
      },
      "chromite" : {
         "quantity" : 1,
         "type" : "chromite"
      },
      "uraninite" : {
         "quantity" : 2,
         "type" : "uraninite"
      }
   },
   "plan" : {
      "Lacuna::DB::Result::Building::Permanent::AmalgusMeadow:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::AmalgusMeadow",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::Beach1:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach1",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::BeeldebanNest:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::DentonBrambles:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::DentonBrambles",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::GeoThermalVent:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::HallsOfVrbansk:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::HallsOfVrbansk",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 32
      },
      "Lacuna::DB::Result::Building::Permanent::KasternsKeep:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::KasternsKeep",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::LapisForest:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::LapisForest",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::MassadsHenge:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::MassadsHenge",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::Ravine:30:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
         "extra_build_level" : 0,
         "level" : 30,
         "quantity" : 6
      },
      "Lacuna::DB::Result::Building::Permanent::TheDillonForge:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::TheDillonForge",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      }
   }
}
