{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 10,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 15,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 15,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Waste",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 15,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:18:45",
               "level" : 10,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2020-11-16 01:18:46",
               "level" : 15,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:18:46",
               "level" : 12,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:18:46",
               "level" : 10,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:46",
               "level" : 15,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:46",
               "level" : 15,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:18:46",
               "level" : 15,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:18:46",
               "level" : 15,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:18:46",
               "level" : 10,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:18:46",
               "level" : 10,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:18:47",
               "level" : 15,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:18:47",
               "level" : 8,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:18:47",
               "level" : 8,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:18:47",
               "level" : 15,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:47",
               "level" : 15,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:48",
               "level" : 15,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:48",
               "level" : 15,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:18:48",
               "level" : 20,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:48",
               "level" : 15,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:48",
               "level" : 15,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:18:48",
               "level" : 15,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:48",
               "level" : 15,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:48",
               "level" : 15,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:49",
               "level" : 15,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:18:49",
               "level" : 15,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2020-11-16 01:18:49",
               "level" : 29,
               "x" : 4,
               "y" : -1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P4",
         "happiness" : 1607459394,
         "id" : 172,
         "is_cap" : 0,
         "name" : "Maia 7",
         "orbit" : 7,
         "x" : -170,
         "y" : -494,
         "zone" : "0|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:19:01",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:19:02",
               "level" : 10,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:19:02",
               "level" : 15,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 01:19:02",
               "level" : 15,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:19:03",
               "level" : 15,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:19:03",
               "level" : 15,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Waste",
               "date_created" : "2020-11-16 01:19:03",
               "level" : 15,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:19:03",
               "level" : 15,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:19:03",
               "level" : 15,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:19:04",
               "level" : 15,
               "x" : -5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 01:19:04",
               "level" : 15,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:04",
               "level" : 15,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:04",
               "level" : 15,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:05",
               "level" : 15,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:05",
               "level" : 15,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:19:05",
               "level" : 15,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 01:19:05",
               "level" : 15,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:19:06",
               "level" : 10,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2020-11-16 01:19:06",
               "level" : 15,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:19:06",
               "level" : 12,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:19:06",
               "level" : 10,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:19:06",
               "level" : 15,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:19:07",
               "level" : 15,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:19:07",
               "level" : 15,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:19:07",
               "level" : 15,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:19:08",
               "level" : 10,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:19:08",
               "level" : 15,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:19:09",
               "level" : 8,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:19:09",
               "level" : 15,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:19:10",
               "level" : 15,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:19:10",
               "level" : 15,
               "x" : 5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:19:10",
               "level" : 15,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:19:11",
               "level" : 15,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:19:11",
               "level" : 20,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:19:11",
               "level" : 15,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:19:11",
               "level" : 15,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:19:11",
               "level" : 15,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:12",
               "level" : 15,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:12",
               "level" : 15,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:12",
               "level" : 15,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:19:12",
               "level" : 15,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:19:13",
               "level" : 21,
               "x" : -2,
               "y" : 0
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P14",
         "happiness" : 792743559949,
         "id" : 9977,
         "is_cap" : 0,
         "name" : "Ja Glezea 7",
         "orbit" : 7,
         "x" : 253,
         "y" : -397,
         "zone" : "1|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:18:19",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:18:19",
               "level" : 10,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:18:19",
               "level" : 15,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 01:18:19",
               "level" : 15,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:18:19",
               "level" : 15,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:18:20",
               "level" : 15,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Waste",
               "date_created" : "2020-11-16 01:18:20",
               "level" : 15,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:20",
               "level" : 15,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:20",
               "level" : 15,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:21",
               "level" : 15,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 01:18:21",
               "level" : 15,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:21",
               "level" : 15,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:21",
               "level" : 15,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:21",
               "level" : 15,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:21",
               "level" : 15,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:18:21",
               "level" : 15,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 01:18:22",
               "level" : 15,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:18:23",
               "level" : 10,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2020-11-16 01:18:23",
               "level" : 15,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:18:24",
               "level" : 12,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:18:25",
               "level" : 10,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:25",
               "level" : 15,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:25",
               "level" : 15,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:18:25",
               "level" : 15,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:18:25",
               "level" : 15,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:18:26",
               "level" : 10,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:18:27",
               "level" : 15,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:18:27",
               "level" : 8,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:18:28",
               "level" : 15,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:28",
               "level" : 15,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:29",
               "level" : 15,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:29",
               "level" : 15,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:18:29",
               "level" : 20,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:29",
               "level" : 15,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:29",
               "level" : 15,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:18:30",
               "level" : 15,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:30",
               "level" : 15,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:30",
               "level" : 15,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:30",
               "level" : 15,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:18:30",
               "level" : 15,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:18:31",
               "level" : 23,
               "x" : -3,
               "y" : 5
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P8",
         "happiness" : 1902314151258,
         "id" : 11520,
         "is_cap" : 0,
         "name" : "Ox Quoetchow 7",
         "orbit" : 7,
         "x" : -499,
         "y" : -377,
         "zone" : "-1|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:18:31",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:18:32",
               "level" : 10,
               "x" : 5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:18:32",
               "level" : 15,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 01:18:32",
               "level" : 15,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:18:32",
               "level" : 15,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:18:33",
               "level" : 15,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Waste",
               "date_created" : "2020-11-16 01:18:33",
               "level" : 15,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:33",
               "level" : 15,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:33",
               "level" : 15,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:33",
               "level" : 15,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 01:18:33",
               "level" : 15,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:34",
               "level" : 15,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:34",
               "level" : 15,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:34",
               "level" : 15,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:34",
               "level" : 15,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:18:34",
               "level" : 15,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 01:18:35",
               "level" : 15,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:18:35",
               "level" : 10,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2020-11-16 01:18:35",
               "level" : 15,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:18:35",
               "level" : 12,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:18:36",
               "level" : 10,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:36",
               "level" : 15,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:36",
               "level" : 15,
               "x" : -4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:18:36",
               "level" : 15,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:18:36",
               "level" : 15,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:18:38",
               "level" : 10,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:18:38",
               "level" : 15,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:18:38",
               "level" : 8,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:18:39",
               "level" : 15,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:39",
               "level" : 15,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:39",
               "level" : 15,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:39",
               "level" : 15,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 20,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 15,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 15,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 15,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 15,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 15,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 15,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 15,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::DentonBrambles",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 23,
               "x" : -2,
               "y" : -4
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P18",
         "happiness" : 1606998999,
         "id" : 50848,
         "is_cap" : 0,
         "name" : "Strui Ruiccae Me 7",
         "orbit" : 7,
         "x" : -257,
         "y" : 33,
         "zone" : "-1|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:19:13",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:19:13",
               "level" : 10,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:19:14",
               "level" : 15,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 01:19:14",
               "level" : 15,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:19:14",
               "level" : 15,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:19:14",
               "level" : 15,
               "x" : 4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Waste",
               "date_created" : "2020-11-16 01:19:14",
               "level" : 15,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:19:15",
               "level" : 15,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:19:15",
               "level" : 15,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:19:15",
               "level" : 15,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 01:19:15",
               "level" : 15,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:15",
               "level" : 15,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 15,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 15,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 15,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 15,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 15,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 10,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 15,
               "x" : 5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 12,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 10,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:19:16",
               "level" : 15,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:19:17",
               "level" : 15,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:19:17",
               "level" : 15,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:19:17",
               "level" : 15,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:19:17",
               "level" : 10,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:19:17",
               "level" : 15,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:19:17",
               "level" : 8,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:19:17",
               "level" : 8,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:19:17",
               "level" : 8,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 20,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 15,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::LapisForest",
               "date_created" : "2020-11-16 01:19:18",
               "level" : 28,
               "x" : 2,
               "y" : -4
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P9",
         "happiness" : 1608095585,
         "id" : 62655,
         "is_cap" : 0,
         "name" : "Llea Ouliand Troa 7",
         "orbit" : 7,
         "x" : 461,
         "y" : 154,
         "zone" : "1|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:18:49",
               "level" : 30,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:18:50",
               "level" : 30,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:18:50",
               "level" : 30,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 01:18:50",
               "level" : 30,
               "x" : -3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:18:51",
               "level" : 30,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:18:51",
               "level" : 30,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:51",
               "level" : 30,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:51",
               "level" : 30,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:52",
               "level" : 30,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 01:18:52",
               "level" : 30,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:52",
               "level" : 30,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:52",
               "level" : 30,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:52",
               "level" : 30,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:53",
               "level" : 30,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:18:53",
               "level" : 30,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 01:18:53",
               "level" : 30,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:18:53",
               "level" : 30,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2020-11-16 01:18:54",
               "level" : 30,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:18:54",
               "level" : 30,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:18:54",
               "level" : 30,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:54",
               "level" : 30,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:55",
               "level" : 30,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:18:55",
               "level" : 30,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:18:55",
               "level" : 30,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:18:56",
               "level" : 30,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:18:57",
               "level" : 30,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:18:57",
               "level" : 30,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:18:58",
               "level" : 30,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:58",
               "level" : 30,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:58",
               "level" : 30,
               "x" : 5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:59",
               "level" : 30,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:18:59",
               "level" : 30,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:59",
               "level" : 30,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:19:00",
               "level" : 30,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:00",
               "level" : 30,
               "x" : 2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:00",
               "level" : 30,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:00",
               "level" : 30,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:19:01",
               "level" : 30,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::PantheonOfHagness",
               "date_created" : "2020-11-16 01:19:01",
               "level" : 30,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Embassy",
               "date_created" : "2020-11-16 05:20:06",
               "level" : 30,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Ministry",
               "date_created" : "2021-04-18 20:52:20",
               "level" : 30,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::MissionCommand",
               "date_created" : "2021-04-18 20:52:56",
               "level" : 30,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Network19",
               "date_created" : "2021-04-18 20:53:34",
               "level" : 30,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SSLa",
               "date_created" : "2021-04-18 20:56:47",
               "level" : 3,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SSLb",
               "date_created" : "2021-04-18 20:57:38",
               "level" : 3,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SSLc",
               "date_created" : "2021-04-18 20:58:23",
               "level" : 3,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SSLd",
               "date_created" : "2021-04-18 20:59:06",
               "level" : 3,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2021-04-18 23:33:20",
               "level" : 30,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2021-04-23 17:04:35",
               "level" : 30,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2021-04-23 17:09:59",
               "level" : 30,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2021-04-23 18:48:20",
               "level" : 30,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2021-04-23 19:10:54",
               "level" : 30,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2021-04-23 19:46:20",
               "level" : 30,
               "x" : 5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2021-04-23 19:48:06",
               "level" : 30,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2021-04-23 19:53:08",
               "level" : 30,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2021-04-23 19:54:23",
               "level" : 30,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Capitol",
               "date_created" : "2021-04-23 20:07:25",
               "level" : 30,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2021-04-23 20:08:56",
               "level" : 30,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::University",
               "date_created" : "2021-04-23 20:10:21",
               "level" : 30,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::TheftTraining",
               "date_created" : "2021-04-23 20:10:48",
               "level" : 30,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::PoliticsTraining",
               "date_created" : "2021-04-23 20:11:48",
               "level" : 30,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::PilotTraining",
               "date_created" : "2021-04-23 20:12:30",
               "level" : 30,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::MercenariesGuild",
               "date_created" : "2021-04-23 20:13:28",
               "level" : 30,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::MayhemTraining",
               "date_created" : "2021-04-23 20:13:43",
               "level" : 30,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::IntelTraining",
               "date_created" : "2021-04-23 20:14:10",
               "level" : 30,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::GeneticsLab",
               "date_created" : "2021-04-23 20:15:02",
               "level" : 30,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::TerraformingLab",
               "date_created" : "2021-04-23 20:16:03",
               "level" : 30,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::GasGiantLab",
               "date_created" : "2021-04-23 20:16:20",
               "level" : 30,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2021-04-23 20:29:51",
               "level" : 30,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2021-04-23 20:29:58",
               "level" : 30,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2021-04-23 20:30:08",
               "level" : 30,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2021-04-23 20:34:08",
               "level" : 30,
               "x" : 4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2021-04-23 20:35:06",
               "level" : 30,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
               "date_created" : "2021-04-23 20:40:39",
               "level" : 30,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2021-04-23 20:41:27",
               "level" : 30,
               "x" : 0,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
               "date_created" : "2021-04-23 20:43:09",
               "level" : 30,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2021-04-23 20:44:42",
               "level" : 30,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2021-04-23 20:47:52",
               "level" : 30,
               "x" : 0,
               "y" : -3
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P7",
         "happiness" : 355343364357,
         "id" : 66733,
         "is_cap" : 1,
         "name" : "Viog 7",
         "orbit" : 7,
         "x" : 94,
         "y" : 201,
         "zone" : "0|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
               "date_created" : "2020-11-15 09:39:19",
               "level" : 30,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2021-04-18 23:15:44",
               "level" : 30,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::PantheonOfHagness",
               "date_created" : "2021-04-23 20:57:08",
               "level" : 30,
               "x" : -5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2021-04-23 21:08:07",
               "level" : 30,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2021-04-23 21:08:28",
               "level" : 30,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-23 21:09:29",
               "level" : 30,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2021-04-23 21:09:43",
               "level" : 30,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2021-04-23 21:13:27",
               "level" : 30,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AmalgusMeadow",
               "date_created" : "2021-04-23 21:13:59",
               "level" : 30,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
               "date_created" : "2021-04-23 21:14:27",
               "level" : 30,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2021-04-23 21:15:13",
               "level" : 30,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2021-04-23 21:15:30",
               "level" : 30,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::DentonBrambles",
               "date_created" : "2021-04-23 21:15:53",
               "level" : 30,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
               "date_created" : "2021-04-23 21:17:25",
               "level" : 30,
               "x" : -3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2021-04-23 21:21:30",
               "level" : 30,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
               "date_created" : "2021-04-23 21:22:06",
               "level" : 30,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::HallsOfVrbansk",
               "date_created" : "2021-04-23 21:22:39",
               "level" : 30,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2021-04-23 21:23:20",
               "level" : 30,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KasternsKeep",
               "date_created" : "2021-04-23 21:25:11",
               "level" : 30,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
               "date_created" : "2021-04-23 21:25:34",
               "level" : 30,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::LapisForest",
               "date_created" : "2021-04-23 21:26:05",
               "level" : 30,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::LibraryOfJith",
               "date_created" : "2021-04-23 21:28:29",
               "level" : 30,
               "x" : 0,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2021-04-23 21:28:51",
               "level" : 30,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MassadsHenge",
               "date_created" : "2021-04-23 21:29:11",
               "level" : 30,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2021-04-23 21:29:43",
               "level" : 30,
               "x" : -1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
               "date_created" : "2021-04-23 21:32:07",
               "level" : 30,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Sand",
               "date_created" : "2021-04-23 21:35:13",
               "level" : 30,
               "x" : -5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2021-04-23 21:35:56",
               "level" : 30,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TempleOfTheDrajilites",
               "date_created" : "2021-04-23 21:38:01",
               "level" : 30,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TheDillonForge",
               "date_created" : "2021-04-23 21:39:55",
               "level" : 30,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
               "date_created" : "2021-04-23 21:52:04",
               "level" : 30,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TerraformingPlatform",
               "date_created" : "2021-04-23 22:09:47",
               "level" : 31,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::SpaceJunkPark",
               "date_created" : "2021-04-23 23:00:59",
               "level" : 30,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2021-04-23 23:01:43",
               "level" : 30,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::JunkHengeSculpture",
               "date_created" : "2021-04-23 23:02:09",
               "level" : 30,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MetalJunkArches",
               "date_created" : "2021-04-23 23:02:31",
               "level" : 30,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::PyramidJunkSculpture",
               "date_created" : "2021-04-23 23:03:03",
               "level" : 30,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTa",
               "date_created" : "2021-04-23 23:09:13",
               "level" : 30,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTb",
               "date_created" : "2021-04-23 23:09:52",
               "level" : 30,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTc",
               "date_created" : "2021-04-23 23:10:07",
               "level" : 30,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTd",
               "date_created" : "2021-04-23 23:10:20",
               "level" : 30,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTe",
               "date_created" : "2021-04-23 23:10:57",
               "level" : 30,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTf",
               "date_created" : "2021-04-23 23:11:16",
               "level" : 30,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTg",
               "date_created" : "2021-04-23 23:11:29",
               "level" : 30,
               "x" : -3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTh",
               "date_created" : "2021-04-23 23:11:38",
               "level" : 30,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::LCOTi",
               "date_created" : "2021-04-23 23:11:56",
               "level" : 30,
               "x" : -5,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2021-04-23 23:34:30",
               "level" : 30,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2021-04-23 23:35:23",
               "level" : 30,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2021-04-24 00:04:14",
               "level" : 30,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2021-04-24 00:11:10",
               "level" : 30,
               "x" : -2,
               "y" : 2
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P20",
         "happiness" : 3598764612169,
         "id" : 66734,
         "is_cap" : 0,
         "name" : "Viog 8",
         "orbit" : 8,
         "x" : 95,
         "y" : 202,
         "zone" : "0|0"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:18:12",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:18:13",
               "level" : 10,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:18:13",
               "level" : 15,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 01:18:13",
               "level" : 15,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:18:13",
               "level" : 15,
               "x" : -4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:18:14",
               "level" : 15,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Waste",
               "date_created" : "2020-11-16 01:18:14",
               "level" : 15,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:14",
               "level" : 15,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:14",
               "level" : 15,
               "x" : -5,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:14",
               "level" : 15,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 01:18:14",
               "level" : 15,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:14",
               "level" : 15,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 15,
               "x" : -5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 15,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 15,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 15,
               "x" : 2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 15,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 10,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 15,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 12,
               "x" : 4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 10,
               "x" : -3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 15,
               "x" : -4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 15,
               "x" : 0,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:18:15",
               "level" : 15,
               "x" : -2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:18:16",
               "level" : 15,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:18:16",
               "level" : 10,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:18:16",
               "level" : 15,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:18:16",
               "level" : 8,
               "x" : 5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:18:17",
               "level" : 15,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:17",
               "level" : 15,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:17",
               "level" : 15,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:17",
               "level" : 15,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:18:17",
               "level" : 20,
               "x" : 3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:18",
               "level" : 15,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:18",
               "level" : 15,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:18:18",
               "level" : 15,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:18",
               "level" : 15,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:18",
               "level" : 15,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:18",
               "level" : 15,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:18:18",
               "level" : 15,
               "x" : -5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:18:18",
               "level" : 13,
               "x" : -2,
               "y" : 3
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P5",
         "happiness" : 606742820,
         "id" : 75290,
         "is_cap" : 0,
         "name" : "Sowly 7",
         "orbit" : 7,
         "x" : -100,
         "y" : 289,
         "zone" : "0|1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:18:40",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 10,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : 4,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Waste",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : 1,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : 1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : -3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : -1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : 0,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:41",
               "level" : 15,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 15,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 15,
               "x" : -5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 15,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 10,
               "x" : -5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 15,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 12,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 10,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 15,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 15,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 15,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:18:42",
               "level" : 15,
               "x" : 1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:18:43",
               "level" : 8,
               "x" : 5,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:18:43",
               "level" : 15,
               "x" : -4,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:43",
               "level" : 15,
               "x" : 5,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:43",
               "level" : 15,
               "x" : 0,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:18:43",
               "level" : 15,
               "x" : -5,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:18:43",
               "level" : 20,
               "x" : 4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:43",
               "level" : 15,
               "x" : 2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 15,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 15,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 15,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 15,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 15,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 15,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GreatBallOfJunk",
               "date_created" : "2020-11-16 01:18:44",
               "level" : 20,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Ministry",
               "date_created" : "2021-04-18 20:11:33",
               "level" : 1,
               "x" : 5,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::GeneticsLab",
               "date_created" : "2021-04-24 05:23:37",
               "level" : 1,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Network19",
               "date_created" : "2021-04-24 07:39:18",
               "level" : 2,
               "x" : 0,
               "y" : 1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P13",
         "happiness" : 511749545160,
         "id" : 76162,
         "is_cap" : 0,
         "name" : "Fra Clarvoe 7",
         "orbit" : 7,
         "x" : -266,
         "y" : 301,
         "zone" : "-1|1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 10,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 15,
               "x" : 3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Hydrocarbon",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 15,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Singularity",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 15,
               "x" : -2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 15,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Waste",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 15,
               "x" : 3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 15,
               "x" : -3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 15,
               "x" : 1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-16 01:19:19",
               "level" : 15,
               "x" : -3,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Burger",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : 2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : 2,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : -2,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : 1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : -1,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Syrup",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 10,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::LuxuryHousing",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 12,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 10,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : -5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : 2,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Refinery",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:19:20",
               "level" : 15,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:19:21",
               "level" : 10,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:19:21",
               "level" : 15,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:19:21",
               "level" : 8,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:19:21",
               "level" : 15,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:19:21",
               "level" : 15,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 15,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Digester",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 15,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 20,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 15,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Treatment",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 15,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::AtmosphericEvaporator",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 15,
               "x" : -4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 15,
               "x" : -5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 15,
               "x" : 1,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Reclamation",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 15,
               "x" : -4,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 15,
               "x" : 1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::LapisForest",
               "date_created" : "2020-11-16 01:19:22",
               "level" : 29,
               "x" : 0,
               "y" : -4
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P2",
         "happiness" : 1606722339,
         "id" : 78800,
         "is_cap" : 0,
         "name" : "Trea Eesno 7",
         "orbit" : 7,
         "x" : 265,
         "y" : 324,
         "zone" : "1|1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:20:13",
      "date_created" : "2020-11-16 01:18:11",
      "essentia" : 4999361,
      "id" : -7,
      "name" : "Diablotin"
   },
   "glyph" : {
      "anthracite" : {
         "quantity" : 6,
         "type" : "anthracite"
      },
      "bauxite" : {
         "quantity" : 8,
         "type" : "bauxite"
      },
      "beryl" : {
         "quantity" : 3,
         "type" : "beryl"
      },
      "chalcopyrite" : {
         "quantity" : 8,
         "type" : "chalcopyrite"
      },
      "chromite" : {
         "quantity" : 3,
         "type" : "chromite"
      },
      "fluorite" : {
         "quantity" : 1,
         "type" : "fluorite"
      },
      "galena" : {
         "quantity" : 12,
         "type" : "galena"
      },
      "goethite" : {
         "quantity" : 13,
         "type" : "goethite"
      },
      "gypsum" : {
         "quantity" : 17,
         "type" : "gypsum"
      },
      "halite" : {
         "quantity" : 1,
         "type" : "halite"
      },
      "magnetite" : {
         "quantity" : 4,
         "type" : "magnetite"
      },
      "methane" : {
         "quantity" : 4,
         "type" : "methane"
      },
      "rutile" : {
         "quantity" : 5,
         "type" : "rutile"
      },
      "sulfur" : {
         "quantity" : 4,
         "type" : "sulfur"
      },
      "trona" : {
         "quantity" : 4,
         "type" : "trona"
      },
      "uraninite" : {
         "quantity" : 1,
         "type" : "uraninite"
      }
   },
   "plan" : {
      "Lacuna::DB::Result::Building::Module::Warehouse:2:0" : {
         "class" : "Lacuna::DB::Result::Building::Module::Warehouse",
         "extra_build_level" : 0,
         "level" : 2,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::AlgaePond:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::AmalgusMeadow:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::AmalgusMeadow",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 3
      },
      "Lacuna::DB::Result::Building::Permanent::Beach11:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::Beach6:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach6",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::BeeldebanNest:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::DentonBrambles:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::DentonBrambles",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::DentonBrambles:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::DentonBrambles",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::Grove:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::LapisForest:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::LapisForest",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 3
      },
      "Lacuna::DB::Result::Building::Permanent::MalcudField:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 2
      },
      "Lacuna::DB::Result::Building::Permanent::NaturalSpring:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 3
      },
      "Lacuna::DB::Result::Building::Permanent::Ravine:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 2
      },
      "Lacuna::DB::Result::Building::Permanent::Volcano:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 2
      }
   }
}
