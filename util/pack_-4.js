{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:24:51",
               "level" : 15,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Waste::Sequestration",
               "date_created" : "2020-11-16 01:24:52",
               "level" : 20,
               "x" : 3,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:24:52",
               "level" : 15,
               "x" : -2,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:24:52",
               "level" : 5,
               "x" : -2,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:24:53",
               "level" : 15,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:24:53",
               "level" : 15,
               "x" : 5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:24:53",
               "level" : 15,
               "x" : 2,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:24:54",
               "level" : 15,
               "x" : 3,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:24:54",
               "level" : 15,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:24:54",
               "level" : 15,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:24:55",
               "level" : 15,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:24:55",
               "level" : 15,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:24:56",
               "level" : 15,
               "x" : -1,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Oversight",
               "date_created" : "2020-11-16 01:24:57",
               "level" : 15,
               "x" : 4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:24:57",
               "level" : 10,
               "x" : -3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 01:24:57",
               "level" : 15,
               "x" : -2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:24:58",
               "level" : 15,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:24:58",
               "level" : 15,
               "x" : -1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:24:58",
               "level" : 15,
               "x" : -4,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::SAW",
               "date_created" : "2020-11-16 01:24:58",
               "level" : 15,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:24:59",
               "level" : 15,
               "x" : 0,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:24:59",
               "level" : 15,
               "x" : -1,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
               "date_created" : "2020-11-16 01:25:00",
               "level" : 15,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:25:00",
               "level" : 15,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:25:00",
               "level" : 15,
               "x" : -1,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
               "date_created" : "2020-11-16 01:25:00",
               "level" : 15,
               "x" : 2,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
               "date_created" : "2020-11-16 01:25:00",
               "level" : 15,
               "x" : 4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:25:00",
               "level" : 15,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
               "date_created" : "2020-11-16 01:25:00",
               "level" : 15,
               "x" : 3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-16 01:25:00",
               "level" : 15,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-16 01:25:00",
               "level" : 15,
               "x" : -4,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Reserve",
               "date_created" : "2020-11-16 01:25:01",
               "level" : 15,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Reserve",
               "date_created" : "2020-11-16 01:25:01",
               "level" : 15,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Corn",
               "date_created" : "2020-11-16 01:25:01",
               "level" : 15,
               "x" : 5,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Wheat",
               "date_created" : "2020-11-16 01:25:01",
               "level" : 15,
               "x" : -4,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Dairy",
               "date_created" : "2020-11-16 01:25:01",
               "level" : 15,
               "x" : 3,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::MissionCommand",
               "date_created" : "2021-04-25 17:14:13",
               "level" : 3,
               "x" : -5,
               "y" : 5
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P11",
         "happiness" : 1000009415578,
         "id" : 39390,
         "is_cap" : 1,
         "name" : "Jusso 3",
         "orbit" : 3,
         "x" : -224,
         "y" : -89,
         "zone" : "0|0"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:19:59",
      "date_created" : "2020-11-16 01:24:50",
      "essentia" : 4947,
      "id" : -4,
      "name" : "Jackpot"
   },
   "glyph" : {
      "anthracite" : {
         "quantity" : 250,
         "type" : "anthracite"
      },
      "bauxite" : {
         "quantity" : 250,
         "type" : "bauxite"
      },
      "beryl" : {
         "quantity" : 250,
         "type" : "beryl"
      },
      "chalcopyrite" : {
         "quantity" : 250,
         "type" : "chalcopyrite"
      },
      "chromite" : {
         "quantity" : 250,
         "type" : "chromite"
      },
      "fluorite" : {
         "quantity" : 250,
         "type" : "fluorite"
      },
      "galena" : {
         "quantity" : 250,
         "type" : "galena"
      },
      "goethite" : {
         "quantity" : 250,
         "type" : "goethite"
      },
      "gold" : {
         "quantity" : 250,
         "type" : "gold"
      },
      "gypsum" : {
         "quantity" : 250,
         "type" : "gypsum"
      },
      "halite" : {
         "quantity" : 250,
         "type" : "halite"
      },
      "kerogen" : {
         "quantity" : 250,
         "type" : "kerogen"
      },
      "magnetite" : {
         "quantity" : 250,
         "type" : "magnetite"
      },
      "methane" : {
         "quantity" : 250,
         "type" : "methane"
      },
      "monazite" : {
         "quantity" : 250,
         "type" : "monazite"
      },
      "rutile" : {
         "quantity" : 250,
         "type" : "rutile"
      },
      "sulfur" : {
         "quantity" : 250,
         "type" : "sulfur"
      },
      "trona" : {
         "quantity" : 250,
         "type" : "trona"
      },
      "uraninite" : {
         "quantity" : 250,
         "type" : "uraninite"
      },
      "zircon" : {
         "quantity" : 250,
         "type" : "zircon"
      }
   },
   "plan" : {
      "Lacuna::DB::Result::Building::DistributionCenter:1:0" : {
         "class" : "Lacuna::DB::Result::Building::DistributionCenter",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Algae:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Algae",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Apple:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Apple",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Bean:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Bean",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Beeldeban:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Beeldeban",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Bread:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Bread",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Burger:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Burger",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Cheese:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Cheese",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Chip:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Chip",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Cider:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Cider",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Corn:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Corn",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::CornMeal:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::CornMeal",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Dairy:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Dairy",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Lapis:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Lapis",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Malcud:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Malcud",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Pancake:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Pancake",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Pie:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Pie",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Potato:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Potato",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Root:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Root",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Shake:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Shake",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Soup:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Soup",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Syrup:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Syrup",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Food::Wheat:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Food::Wheat",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::LCOTb:1:0" : {
         "class" : "Lacuna::DB::Result::Building::LCOTb",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::LCOTd:1:0" : {
         "class" : "Lacuna::DB::Result::Building::LCOTd",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::LCOTe:1:0" : {
         "class" : "Lacuna::DB::Result::Building::LCOTe",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::LCOTi:1:0" : {
         "class" : "Lacuna::DB::Result::Building::LCOTi",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      },
      "Lacuna::DB::Result::Building::Permanent::AlgaePond:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::AlgaePond:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::AmalgusMeadow:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::AmalgusMeadow",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::AmalgusMeadow:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::AmalgusMeadow",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::Beach10:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach10",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach10:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach10",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach10:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach10",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach10:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach10",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach10:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach10",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach10:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach10",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach10:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach10",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach10:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach10",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach11:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach11:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach11:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach11:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach11:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach11:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach11:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach11:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach11",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach12:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach12",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach12:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach12",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach12:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach12",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach12:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach12",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach12:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach12",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach12:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach12",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach12:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach12",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach12:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach12",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach13:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach13",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach1:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach1",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach1:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach1",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach1:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach1",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach1:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach1",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach1:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach1",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach1:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach1",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach1:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach1",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach1:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach1",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach2:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach2",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach2:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach2",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach2:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach2",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach2:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach2",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach2:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach2",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach2:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach2",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach2:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach2",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach2:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach2",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach3:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach3",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach3:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach3",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach3:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach3",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach3:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach3",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach3:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach3",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach3:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach3",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach3:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach3",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach3:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach3",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach4:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach4:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach4:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach4:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach4:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach4:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach4:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach4:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach4",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach5:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach5:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach5:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach5:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach5:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach5:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach5:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach5:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach6:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach6",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach6:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach6",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach6:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach6",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach6:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach6",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach6:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach6",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach6:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach6",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach6:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach6",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach6:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach6",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach7:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach7",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach7:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach7",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach7:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach7",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach7:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach7",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach7:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach7",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach7:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach7",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach7:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach7",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach7:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach7",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach8:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach8",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach8:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach8",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach8:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach8",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach8:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach8",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach8:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach8",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach8:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach8",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach8:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach8",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach8:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach8",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach9:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach9",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach9:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach9",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach9:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach9",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach9:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach9",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach9:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach9",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach9:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach9",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach9:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach9",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Beach9:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach9",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::BeeldebanNest:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::BeeldebanNest:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::BeeldebanNest",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::BlackHoleGenerator",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::CitadelOfKnope",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::CrashedShipSite:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::CrashedShipSite:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::Crater:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Crater:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Crater:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Crater:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Crater:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Crater:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Crater:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Crater:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::DentonBrambles:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::DentonBrambles",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::DentonBrambles:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::DentonBrambles",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::GeoThermalVent:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::GeoThermalVent:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::GratchsGauntlet",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::Grove:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Grove:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Grove:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Grove:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Grove:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Grove:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Grove:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Grove:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Grove",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::HallsOfVrbansk:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::HallsOfVrbansk",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 50
      },
      "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::KalavianRuins:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::KalavianRuins:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::KalavianRuins",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::Lagoon:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lagoon:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lagoon:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lagoon:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lagoon:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lagoon:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lagoon:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lagoon:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lagoon",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lake:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lake:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lake:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lake:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lake:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lake:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lake:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Lake:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::LapisForest:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::LapisForest",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::LapisForest:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::LapisForest",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::LibraryOfJith:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::LibraryOfJith",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::LibraryOfJith:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::LibraryOfJith",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::MalcudField:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::MalcudField:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::MalcudField",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::NaturalSpring:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::NaturalSpring:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::OracleOfAnid:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::OracleOfAnid:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::OracleOfAnid",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::PantheonOfHagness:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::PantheonOfHagness",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::PantheonOfHagness:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::PantheonOfHagness",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::Ravine:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::Ravine:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Ravine",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::RockyOutcrop:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::RockyOutcrop:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::RockyOutcrop:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::RockyOutcrop:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::RockyOutcrop:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::RockyOutcrop:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::RockyOutcrop:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::RockyOutcrop:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Sand:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Sand",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Sand:1:1" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Sand",
         "extra_build_level" : 1,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Sand:1:2" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Sand",
         "extra_build_level" : 2,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Sand:1:3" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Sand",
         "extra_build_level" : 3,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Sand:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Sand",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Sand:1:5" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Sand",
         "extra_build_level" : 5,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Sand:1:6" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Sand",
         "extra_build_level" : 6,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::Sand:1:7" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Sand",
         "extra_build_level" : 7,
         "level" : 1,
         "quantity" : 250
      },
      "Lacuna::DB::Result::Building::Permanent::TempleOfTheDrajilites:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::TempleOfTheDrajilites",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::TempleOfTheDrajilites:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::TempleOfTheDrajilites",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::Permanent::TerraformingPlatform:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::TerraformingPlatform",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::Volcano:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 25
      },
      "Lacuna::DB::Result::Building::Permanent::Volcano:1:4" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
         "extra_build_level" : 4,
         "level" : 1,
         "quantity" : 5
      },
      "Lacuna::DB::Result::Building::SAW:1:0" : {
         "class" : "Lacuna::DB::Result::Building::SAW",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 10
      },
      "Lacuna::DB::Result::Building::SAW:1:9" : {
         "class" : "Lacuna::DB::Result::Building::SAW",
         "extra_build_level" : 9,
         "level" : 1,
         "quantity" : 1
      }
   }
}
