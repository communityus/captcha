{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 01:14:08",
               "level" : 30,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::CrashedShipSite",
               "date_created" : "2020-11-16 01:14:08",
               "level" : 28,
               "x" : 5,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::NaturalSpring",
               "date_created" : "2020-11-16 01:14:09",
               "level" : 27,
               "x" : 0,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::TheDillonForge",
               "date_created" : "2020-11-16 01:14:09",
               "level" : 30,
               "x" : 4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::CloakingLab",
               "date_created" : "2020-11-16 01:14:09",
               "level" : 27,
               "x" : -4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Security",
               "date_created" : "2020-11-16 01:14:09",
               "level" : 27,
               "x" : -1,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Shipyard",
               "date_created" : "2020-11-16 01:14:09",
               "level" : 30,
               "x" : 3,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Development",
               "date_created" : "2020-11-16 01:14:11",
               "level" : 15,
               "x" : 1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Espionage",
               "date_created" : "2020-11-16 01:14:11",
               "level" : 27,
               "x" : -3,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::SpacePort",
               "date_created" : "2020-11-16 01:14:21",
               "level" : 27,
               "x" : 4,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::InterDimensionalRift",
               "date_created" : "2020-11-16 01:14:24",
               "level" : 28,
               "x" : 5,
               "y" : -1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Archaeology",
               "date_created" : "2020-11-16 01:14:25",
               "level" : 20,
               "x" : -4,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Propulsion",
               "date_created" : "2020-11-16 01:14:25",
               "level" : 28,
               "x" : 3,
               "y" : 2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Transporter",
               "date_created" : "2020-11-16 01:14:26",
               "level" : 25,
               "x" : 4,
               "y" : -2
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GeoThermalVent",
               "date_created" : "2020-11-16 01:14:27",
               "level" : 28,
               "x" : 2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Trade",
               "date_created" : "2020-11-16 01:14:27",
               "level" : 25,
               "x" : 0,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::MunitionsLab",
               "date_created" : "2020-11-16 01:14:28",
               "level" : 20,
               "x" : -2,
               "y" : 3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Intelligence",
               "date_created" : "2020-11-16 01:14:28",
               "level" : 27,
               "x" : 4,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::AlgaePond",
               "date_created" : "2020-11-16 01:14:29",
               "level" : 30,
               "x" : 5,
               "y" : -5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Volcano",
               "date_created" : "2020-11-16 01:14:29",
               "level" : 26,
               "x" : 3,
               "y" : -3
            },
            {
               "class" : "Lacuna::DB::Result::Building::Observatory",
               "date_created" : "2020-11-16 01:14:31",
               "level" : 15,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::GasGiantLab",
               "date_created" : "2021-04-18 19:02:11",
               "level" : 30,
               "x" : -5,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform",
               "date_created" : "2021-04-18 19:02:39",
               "level" : 16,
               "x" : -4,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform",
               "date_created" : "2021-04-18 19:02:57",
               "level" : 16,
               "x" : -3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform",
               "date_created" : "2021-04-18 19:03:12",
               "level" : 11,
               "x" : -2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform",
               "date_created" : "2021-04-18 19:05:01",
               "level" : 15,
               "x" : -1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform",
               "date_created" : "2021-04-18 19:05:19",
               "level" : 15,
               "x" : 0,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform",
               "date_created" : "2021-04-18 19:05:35",
               "level" : 10,
               "x" : 1,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform",
               "date_created" : "2021-04-18 19:05:51",
               "level" : 10,
               "x" : 2,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::GasGiantPlatform",
               "date_created" : "2021-04-18 19:06:06",
               "level" : 10,
               "x" : 3,
               "y" : 5
            },
            {
               "class" : "Lacuna::DB::Result::Building::University",
               "date_created" : "2021-04-18 19:13:53",
               "level" : 30,
               "x" : 5,
               "y" : 5
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::GasGiant::G5",
         "happiness" : -926219269350,
         "id" : 46,
         "is_cap" : 1,
         "name" : "Rozeske 8",
         "orbit" : 8,
         "x" : -424,
         "y" : -493,
         "zone" : "-1|-1"
      },
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2021-04-25 18:34:48",
               "level" : 1,
               "x" : 0,
               "y" : 0
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P14",
         "happiness" : 0,
         "id" : 365,
         "is_cap" : 0,
         "name" : "TLE DeLambert 5",
         "orbit" : 2,
         "x" : 254,
         "y" : -496,
         "zone" : "1|-1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:20:20",
      "date_created" : "2020-11-16 01:14:08",
      "essentia" : 0,
      "id" : -9,
      "name" : "DeLambert"
   },
   "glyph" : {
      "goethite" : {
         "quantity" : 4,
         "type" : "goethite"
      },
      "sulfur" : {
         "quantity" : 5,
         "type" : "sulfur"
      }
   },
   "plan" : {
      "Lacuna::DB::Result::Building::Permanent::Beach5:1:0" : {
         "class" : "Lacuna::DB::Result::Building::Permanent::Beach5",
         "extra_build_level" : 0,
         "level" : 1,
         "quantity" : 1
      }
   }
}
