{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Crater",
               "date_created" : "2020-11-15 09:18:58",
               "level" : 1,
               "x" : 1,
               "y" : 4
            },
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-15 23:59:26",
               "level" : 4,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Corn",
               "date_created" : "2020-11-16 00:46:48",
               "level" : 1,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-16 00:47:54",
               "level" : 1,
               "x" : -2,
               "y" : 1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P15",
         "happiness" : 0,
         "id" : 16773,
         "is_cap" : 1,
         "name" : "Oak Viefre Ghai 3",
         "orbit" : 3,
         "x" : 496,
         "y" : -327,
         "zone" : "1|-1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:18:55",
      "date_created" : "2020-11-15 23:59:21",
      "essentia" : 100,
      "id" : 4,
      "name" : "VivaNovaLux"
   },
   "glyph" : {},
   "plan" : {}
}
