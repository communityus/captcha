{
   "body" : [
      {
         "building" : [
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::RockyOutcrop",
               "date_created" : "2020-11-15 09:12:12",
               "level" : 1,
               "x" : -3,
               "y" : -4
            },
            {
               "class" : "Lacuna::DB::Result::Building::Permanent::Lake",
               "date_created" : "2020-11-15 09:12:12",
               "level" : 1,
               "x" : -1,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::PlanetaryCommand",
               "date_created" : "2020-11-16 00:10:08",
               "level" : 4,
               "x" : 0,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Malcud",
               "date_created" : "2020-11-23 17:36:35",
               "level" : 2,
               "x" : -1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Purification",
               "date_created" : "2020-11-23 17:39:35",
               "level" : 3,
               "x" : 0,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Water::Storage",
               "date_created" : "2020-11-23 17:40:57",
               "level" : 1,
               "x" : 1,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Mine",
               "date_created" : "2020-11-23 17:43:16",
               "level" : 2,
               "x" : -2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Ore::Storage",
               "date_created" : "2020-11-23 17:46:38",
               "level" : 1,
               "x" : -3,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::Energy::Geo",
               "date_created" : "2020-11-23 17:48:30",
               "level" : 1,
               "x" : 2,
               "y" : 1
            },
            {
               "class" : "Lacuna::DB::Result::Building::University",
               "date_created" : "2020-11-23 17:55:45",
               "level" : 4,
               "x" : -2,
               "y" : 0
            },
            {
               "class" : "Lacuna::DB::Result::Building::Food::Algae",
               "date_created" : "2020-11-23 18:22:10",
               "level" : 2,
               "x" : -4,
               "y" : 1
            }
         ],
         "class" : "Lacuna::DB::Result::Map::Body::Planet::P13",
         "happiness" : 0,
         "id" : 1334,
         "is_cap" : 1,
         "name" : "Regulus 3",
         "orbit" : 3,
         "x" : 256,
         "y" : -488,
         "zone" : "1|-1"
      }
   ],
   "empire" : {
      "archive_date" : "2021-04-25 21:18:28",
      "date_created" : "2020-11-16 00:10:05",
      "essentia" : 50,
      "id" : 6,
      "name" : "RevisedKnight48"
   },
   "glyph" : {},
   "plan" : {}
}
